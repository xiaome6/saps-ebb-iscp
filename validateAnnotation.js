function validateAnnotation(text){

    var validateAreas = document.getElementById("features");
    
     var features = "";
     var linkers = [];
     var fixed_linkers=[];
     var localIds=[];
    
     var plasmidName = '<div id="plasmidName">' + 
     '<label>Plasmid Name: </label>' +
     '<input type="text" value="">' + 
     '</div>';

     features += plasmidName;

     for(var i=0; i<text.matches.length; i++){
         var localId = text.matches[i].region.localId;
         var start = text.matches[i].sequence.aaMin + 1;
         var stop = text.matches[i].sequence.aaMax;
         frameStart = text.dnaMin;

         if(localId.includes("linker")){
             linkers.push([start, stop]);
             fixed_linkers.push([start, stop]);
         }

         if(!localId.includes("linker")){
             localIds.push("feature_" + localId);
         var feature = ' <div id="feature_' +localId + '">' + 
           '   <label>Feature Name: </label> ' + 
           '   <input type="text" value= ' + localId + '>' +
           '   <label>    Start: </label> ' + 
           '   <input type="text" id="feature_' + localId + '_startPos"' + ' value= ' + start + '>' +
           '   <label>    Stop: </label> ' +
           '   <input type="text" id="feature_' + localId + '_stopPos"' + ' value= ' + stop + '>' +
           '   <input type="button" value="Highlight" id="feature_' + localId + '" onclick="highLightSequence(this.id);">' +
           '   <input type="button" value="+" id="feature_' + localId + '_ButtonAdd" onclick="addOneAnnotation(this.id);">' + 
           '   <input type="button" value="-" id="feature_' + localId + '_ButtonMinus" onclick="removeOneAnnotation(this.id);">' + 
           '   </div> ' ;

           features += feature;
         }
     }


    var sorted_linkers = linkers.sort(function(a, b) {
        return  a[0] > b[0] ? 1 : -1;
    });
    
    for(var linker=0; linker<sorted_linkers.length; linker++){

        var start = sorted_linkers[linker][0];
        var stop = sorted_linkers[linker][1];

        for(var count=0; count<fixed_linkers.length; count++){
            var fixed_start = fixed_linkers[count][0];
            var fixed_stop = fixed_linkers[count][1];

            if(start <= fixed_start && stop >= fixed_stop){

            }
            else if(start > fixed_start && stop < fixed_stop){
                start = fixed_start;
                stop = fixed_stop;
            }
            else if(start <= fixed_start && stop < fixed_stop && stop >= fixed_start){
                stop = fixed_stop;
            }
            else if(start > fixed_start && stop >= fixed_stop && start <=fixed_stop){
                start = fixed_start;
            }
        }
        sorted_linkers[linker][0] = start;
        sorted_linkers[linker][1] = stop;
    }

    unique_array = Array.from(new Set(sorted_linkers.map(JSON.stringify)), JSON.parse)

    for(var i=0; i<unique_array.length; i++)
    {
        var linkerName = "feature_linker_" + i;
        var linkerValue = "linker_" + i;
        startDNA = text.dnaMin + (unique_array[i][0]-1)*3;
        stopDNA = text.dnaMin + unique_array[i][1]*3;

        localIds.push(linkerName);
        var feature = ' <div id="' + linkerName + '">' + 
           '   <label>Feature Name: </label> ' + 
           '   <input type="text" value= ' + linkerValue + '>' +
           '   <label>    Start: </label> ' + 
           '   <input type="text" id="'+linkerName + '_startPos"' + ' value= ' + unique_array[i][0] + '>' +
           '   <label>    Stop: </label> ' +
           '   <input type="text" id="' + linkerName + '_stopPos"' + ' value= ' + unique_array[i][1] + '>' +
           '   <input type="button" value="Highlight" id="' + linkerName + '" onclick="highLightSequence(this.id);">' + 
           '   <input type="button" value="+" id="' + linkerName + '_ButtonAdd" onclick="addOneAnnotation(this.id);">' + 
           '   <input type="button" value="-" id="' + linkerName + '_ButtonMinus" onclick="removeOneAnnotation(this.id);">' + 
           '   </div> ' ;

           features += feature;
    }
    
    
    validateAreas.innerHTML = features;   
    
}

function addOneAnnotation(button_id){
    var divId = button_id.replace("_ButtonAdd", "");
    var annotationDiv = document.getElementById(divId);
    var newFeatureName = divId + elementID;
    elementID ++;
    var newFeature = ' <div id="' + newFeatureName + '">' + 
    ' <label>Feature Name: </label> ' + 
    ' <input type="text">' +
    ' <label>    Start: </label> ' + 
     '    <input type="text" id="' + newFeatureName + '_startPos"' +  '>' +
      '   <label>    Stop: </label> ' +
      '   <input type="text" id="' + newFeatureName + '_stopPos"' + '>' + 
      '   <input type="button" value="Highlight" id="' + newFeatureName + '" onclick="highLightSequence(this.id);"> ' +
      '   <input type="button" value="+" id="' + newFeatureName + '_ButtonAdd" onclick="addOneAnnotation(this.id);">' + 
      '   <input type="button" value="-" id="' + newFeatureName + '_ButtonMinus" onclick="removeOneAnnotation(this.id);">' + 
      '   </div> ' ;
      annotationDiv.insertAdjacentHTML('afterend', newFeature);
   
}

function removeOneAnnotation(button_id){
    var annotationDiv = document.getElementById(button_id.replace("_ButtonMinus",""));
    annotationDiv.remove();
}

function highLightSequence(button_id){
    startPos = document.getElementById(button_id + "_startPos");
    stopPos = document.getElementById(button_id + "_stopPos");
    document.getElementById("linear_vis").innerHTML = "";
    showDNA(startPos.value, stopPos.value);
}