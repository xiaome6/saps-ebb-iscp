"use strict";

var app = angular.module('vixis', ['ngSanitize', 'vr.directives.slider', 'ui.bootstrap','angularplasmid']);

app.controller('PlasmidCtrl', ['$http', '$scope', '$timeout', function ($http, $scope, $timeout) {
    
    $scope.featurePos = 216;
    
    $scope.updateCircularVis = function(){
        $scope.sequence = inputSequence;
        $scope.gbFile = circularName;


    var allFeatures = document.getElementById('features').children;

    var newFeatures = [];
    var newLinkers = [];
    

    for(var i=1; i<allFeatures.length; i++){
        var featureName = allFeatures[i].children[1].value;
        var featureStart = (allFeatures[i].children[3].value - 1) *3 + frameStart;
        var featureStop = allFeatures[i].children[5].value*3 + frameStart;
        var newFeature = {};
        newFeature.start = featureStart;
        newFeature.end = featureStop;
        newFeature.text = featureName;
        if(featureName.includes("linker")){
            newLinkers.push(newFeature);
        }else{
            newFeatures.push(newFeature);
        }
    }

    
    var genbankOriginalFeatures = (JSON.parse(genbankFeatures)).features;
    for(var i=0; i<genbankOriginalFeatures.length; i++){
        if(typeof genbankOriginalFeatures[i].product != 'undefined'){
            var origFeature = {};
            var tmp_start = genbankOriginalFeatures[i].location[0].start;
            var tmp_end = genbankOriginalFeatures[i].location[0].end;
            if(tmp_start > tmp_end){
                origFeature.start = tmp_end;
                origFeature.end = tmp_start;
            } else{
                origFeature.start = tmp_start;
                origFeature.end = tmp_end;
            }
            
            origFeature.text = genbankOriginalFeatures[i].product;
            newFeatures.push(origFeature);
        } 
    }

    $scope.featureMarkers = newFeatures;
    $scope.linkerMarkers = newLinkers;
    
        $scope.featureStart = $scope.featureMarkers[0].start - 20;
        $scope.featureEnd = $scope.featureMarkers[$scope.featureMarkers.length-1].end + 20;
                
        
    };

    /*
    $scope.sequenceRegion = {
        start : 802,
        end : 980
    };
        
    
    $scope.markerClick = function(event, marker){
        $scope.$apply(function(){
            $scope.selectedmarker = marker;        
        });
    }
    
    $scope.dismissPopup = function(){
        $scope.selectedmarker = null;    
    }
    
    $scope.animate = function(){
            
        if ($scope.enableAnimation) {
            var step = 2;
            $timeout(function(){
                angular.forEach($scope.featureMarkers,function(marker){
                    marker.start = (marker.start>1000) ? marker.start-1000  : marker.start + step;
                    marker.end = (marker.end>1000) ? marker.end-1000 : marker.end + step;
                })

                $scope.featureStart = $scope.featureMarkers[0].start-20;
                $scope.featureEnd = $scope.featureMarkers[$scope.featureMarkers.length-1].end+20;
                $scope.animate();
            },20);
        }
    }
*/

}]);
