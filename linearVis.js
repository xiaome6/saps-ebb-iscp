function setLinearVis(startHighlight, stopHighlight){
    var linearVisDiv = document.getElementById("linear_vis");
    var fourspaces = "&nbsp;&nbsp;&nbsp;&nbsp;"
    var matureProtein = testing_mature_sequence.split('').map(formatAA).join('');
    var matureSequence = testing_mature_dnasequence.toUpperCase();
    var complementSeq = matureSequence.split('').reverse().map(complement).join('');
    
    var blockLength = 30;
    var blocksPerRow = 5;

    var hightlightStartPos = (startHighlight - 1) * 3;
    var hightlightStopPos = (stopHighlight) * 3;

    var content = "<p>";
    var rowCount = matureProtein.length / (blockLength*blocksPerRow);
    for(var i=0; i<rowCount; i++){

        var rowValue = "";
        var digit_value = (i*50+1).toString();
        var digits = 10 - digit_value.length;
        var padding = "";
        for(var pad = 0; pad < digits; pad ++){
            padding +=  "&nbsp;";
        }
        rowValue += digit_value + padding;
        

        content += rowValue + fourspaces;
        for(var j=0; j<blockLength*blocksPerRow; j++){
            var elementIndex = i*blockLength*blocksPerRow + j;
            var element = matureProtein[elementIndex];
            if(j%blockLength == 0){
                content += fourspaces;
            }
            if(element != undefined)
            {
                if(elementIndex >= hightlightStartPos && elementIndex < hightlightStopPos)
                {
                    content += "<font color=red>" + element + "</font>";
                }
                else{
                    content += element;
                }
            }
        }
        content += "<br>";
        content += rowValue + fourspaces;
        for(var j=0; j<blockLength*blocksPerRow; j++){
            var elementIndex = i*blockLength*blocksPerRow + j;
            var element = matureSequence[elementIndex];
            if(j%blockLength == 0){
                content += fourspaces;
            }
            if(element != undefined)
            {
                if(elementIndex >= hightlightStartPos && elementIndex < hightlightStopPos)
                {
                    content += "<font color=red>" + element + "</font>";
                }
                else{
                    content += element;
                }
            }
        }
        content += "<br>";
        content += rowValue + fourspaces;
        for(var j=0; j<blockLength*blocksPerRow; j++){
            var elementIndex = i*blockLength*blocksPerRow + j;
            var element = complementSeq[elementIndex];
            if(j%blockLength == 0){
                content += fourspaces;
            }
            if(element != undefined)
            { 
                if(elementIndex >= hightlightStartPos && elementIndex < hightlightStopPos)
                {
                    content += "<font color=red>" + element + "</font>";
                }
                else{
                    content += element;
                }
            }
        }
        content += "<p>";
    }
    

    linearVisDiv.innerHTML = "<pre>"  + content + "</pre>";

    }

    function complement(a) {
        return { A: 'T', T: 'A', G: 'C', C: 'G' }[a];
    } 

    function formatAA(a){
        return " " + [a] + " ";
    }

    function read(sequence, start, stop){

    }

    
    function setAALinearVis(startHighlight, stopHighlight){
        var linearVisDiv = document.getElementById("linear_vis");
        var fourspaces = "&nbsp;&nbsp;&nbsp;&nbsp;"
        var matureProtein = testing_mature_sequence;
       
        var blockLength = 10;
        var blocksPerRow = 5;
    
        var hightlightStartPos = startHighlight - 1 ;
        var hightlightStopPos = stopHighlight;
    
        var content = "<p>";
        var rowCount = matureProtein.length / (blockLength*blocksPerRow);
        for(var i=0; i<rowCount; i++){
    
            var rowValue = "";
            var digit_value = (i*50+1).toString();
            var digits = 10 - digit_value.length;
            var padding = "";
            for(var pad = 0; pad < digits; pad ++){
                padding +=  "&nbsp;";
            }
            rowValue += digit_value + padding;
            
    
            content += rowValue + fourspaces;
            for(var j=0; j<blockLength*blocksPerRow; j++){
                var elementIndex = i*blockLength*blocksPerRow + j;
                var element = matureProtein[elementIndex];
                if(j%blockLength == 0){
                    content += fourspaces;
                }
                if(element != undefined)
                {
                    if(elementIndex >= hightlightStartPos && elementIndex < hightlightStopPos)
                    {
                        content += "<font color=red>" + element + "</font>";
                    }
                    else{
                        content += element;
                    }
                }
            }
            content += "<p>";
        }
        
    
        linearVisDiv.innerHTML = "<pre>"  + content + "</pre>";
    
        }
    