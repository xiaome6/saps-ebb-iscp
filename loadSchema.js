function loadschema(){
    fetch('https://saps-svc.dev.nibr.novartis.net/annotations/v1/schemes')
    .then(function(response) {
                    return response.text();
                    })
                    .then(function(text) {
                        var select_schemas = document.getElementById("select_schemas");
                        var tmp = text.replace(/\[|\]|"/g, "").split(",");
                        for(var i=0; i<tmp.length; i++){
                            var select_schemas_option = document.createElement("option");
                            select_schemas_option.innerHTML = tmp[i];
                            select_schemas.appendChild(select_schemas_option);
                        }

                    console.log('Request successful', text);
                    })
                    .catch(function(error) {
                    log('Request failed', error)
                    });
    }

    var schemas = loadschema();