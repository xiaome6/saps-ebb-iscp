/* This is the input to the generic annotation service */
var inputSequence = '';  
var inputString = ' "polymerType": "DNA", ' + 
  ' "id":"firstNBCTest", ' + 
  ' "label":"firstNBCTest", ' + 
  ' "circular": false, ' + 
  ' "metadata": { ' + 
  ' "species": "synthetic" ' + 
  ' }, ' + 
  ' "data":{ ' + 
  '    "SEQANNOT":{ ' +
  '        "input":{ ' + 
  '            "inlineRegions":true ' +
  '        }, ' +
  '        "scheme":"defaultschema", ' +
  '        "dnamode":"plasmid" ' +
  '   } ' +
      
  ' } ' +
  ' } ' ;
var elementID = 0;
var testing_mature_sequence = "";
var testing_mature_dnasequence = "";
var testing_input_sequence = "";
var candidateChain = "";
var matches = "";
var genBankFeatures = "";
var circularName = "testing";
var genBankContents = "";
var frameStart = 0;