function downloadGBFile(){    

    var allFeatures = document.getElementById('features').children;

    var newFeatures = "";

    for(var i=1; i<allFeatures.length; i++){
        var featureName = allFeatures[i].children[1].value;
        var featureStart = (allFeatures[i].children[3].value - 1) *3 + frameStart;
        var featureStop = allFeatures[i].children[5].value*3 + frameStart;
        newFeatures += "     " + "misc_feature" + "    " + featureStart + ".." + featureStop + "\n";
        newFeatures += "                     " + "\/product=\"" + featureName +  "\"\n";
        newFeatures += "                     " + "\/label=\"" + featureName +  "\"\n";
    }

    var newGenBankContents = genBankContents.replace("BASE", newFeatures + "\nBASE");
    var data = new Blob([newGenBankContents]);
   
    var a = document.getElementById('download');
    a.href = URL.createObjectURL(data);
    var finalFileName = document.getElementById('finalGBFileName').value;
    document.getElementById('download').download=finalFileName;
    document.getElementById("download").click();
}