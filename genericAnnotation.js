function annotate(){

    var e = document.getElementById("select_schemas");
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;

    var inputSchema = inputString.replace("defaultschema", value);
    var inputJson = inputSequence + inputSchema;
    

    fetch('https://saps-svc.dev.nibr.novartis.net/annotations/v1/commands/annotateSaps', {
        method: 'POST',
        mode: 'cors',
        headers: {'Content-Type': 'application/json'},
        body: inputJson
    })
    .then(function(response) {
    return response.text();
    })
    .then(function(text) {
    parseAnnotation(text);
    console.log('Request successful', text);
    })
    .catch(function(error) {
    log('Request failed', error)
    });
}

document.getElementById("genericAnnotation").addEventListener('click', annotate);

function parseAnnotation(text){
    annotationJson = JSON.parse(text);
    var number_of_chains = annotationJson.data.SEQANNOT.sequences.sequences[0].chains.length;
    var candidate = -1;
    var candidateLength = -1;
    for(var i=0; i<number_of_chains; i++){
        var chain = annotationJson.data.SEQANNOT.sequences.sequences[0].chains[i];
        var matches = annotationJson.data.SEQANNOT.sequences.sequences[0].chains[i].matches.length;
        if(chain.dnaFrame.includes("+") && chain.length > candidateLength){
            candidate = i;
            candidateLength = chain.length;
        }
    }
    testing_input_sequence = annotationJson.sequence;
    testing_mature_sequence = annotationJson.data.SEQANNOT.sequences.sequences[0].chains[candidate].residues;
    testing_mature_dnasequence = annotationJson.data.SEQANNOT.sequences.sequences[0].chains[candidate].dnaSequence;
    candidateChain = annotationJson.data.SEQANNOT.sequences.sequences[0].chains[candidate],
    matches = annotationJson.data.SEQANNOT.sequences.sequences[0].matches
    validateAnnotation(annotationJson.data.SEQANNOT.sequences.sequences[0].chains[candidate]);
    setAALinearVis(-1, -1);
}