function showDNA(startPos, stopPos){
    document.getElementById("linear_vis").innerHTML = "";
    var checkBox = document.getElementById("showDNA");
    if(checkBox.checked == true){
        setLinearVis(startPos, stopPos);
    } else{
        setAALinearVis(startPos, stopPos);
    }
}